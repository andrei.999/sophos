import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../_reducers';
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../_sagas';

let store: any = null
const sagaMiddleware = createSagaMiddleware();
store = createStore(
        rootReducer,
        undefined,
        compose(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga)

export const getStore = () => {
        return store;
}

export default store;