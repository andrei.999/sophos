import { Console } from 'console';
import React from 'react';
import './Calculator.css';


class Calculator extends React.Component<any, any>{
    public priceBTC: number = 10593;
    public USD: number = (1 / 10593);
    public amount: number = 0;
    public convert: any = "convert1";
    public moneda1: any = "";
    public moneda2: any = "";
    constructor(props: any) {
        super(props)
        this.state = {
            moneda1: "",
            moneda2: "",
            changeResult: 0,
        }

        this.executeChange = this.executeChange.bind(this);
        this.reverseCurrent = this.reverseCurrent.bind(this);
    }



    public executeChange(convert: any, name: any, event: any) {

        if (name === "amount") {
            this.amount = event.target.value;
        }

        if (name === "moneda1") {
            this.moneda1 = name;
        }

        if (name === "moneda2") {
            this.moneda2 = name;
        }

        if (this.moneda1 !== "" && this.moneda2 !== "") {

            console.log("ENTRO EN IFFF")

            switch (this.convert) {
                case "convert1":
                    this.setState({ changeResult: (this.amount * this.priceBTC) })
                    break;
                case "convert2":
                    this.setState({ changeResult: (this.amount * this.USD) })
                    break;
            }
        }

    }

    public reverseCurrent() {
        if (this.convert == "convert2") {
            this.convert = "convert1";
        } else {
            this.convert = "convert2";
        }
        this.executeChange(this.amount, null, null);
    }

    public render() {
        const { changeResult } = this.state;

        console.log(changeResult);

        return (
            <div className="content-calculator">
                <div className="titles">
                    <p className="title-1">Realizar Cambio</p>
                    <p className="subtitle">Seleccione las monedas</p>
                </div>

                <div className="inputs-calculator">
                    <div className="inputs">
                        <input className="inputs-calc" type="text" placeholder={"0,00"} name={"amount"} onChange={this.executeChange.bind(this, "", "amount")} />
                        <select className="inputs-calc select" name="moneda1" id="moneda1" onChange={this.executeChange.bind(this, "", "moneda1")}>
                            <option selected value="">-- Seleccione --</option>
                            <option value="BTC">BTC</option>
                        </select>

                        <select className="inputs-calc select" name="moneda2" id="moneda2" onChange={this.executeChange.bind(this, "", "moneda2")}>
                            <option selected value="">-- Seleccione --</option>
                            <option value="USD">USD</option>
                        </select>

                        <input className="inputs-calc" type="text" placeholder={"0,00"} value={this.state.changeResult} readOnly disabled={true} />

                    </div>
                    <div className="btn-reset" onClick={this.reverseCurrent}>
                        <svg width="35px" height="35px" viewBox="0 0 16 16" className="bi bi-arrow-repeat" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z" />
                            <path fillRule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z" />
                        </svg>
                    </div>
                </div>

            </div>
        )
    }


}

export default Calculator;
