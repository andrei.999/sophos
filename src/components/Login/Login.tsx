import React from 'react';
import './Login.css';
import LoginImg from '../../assets/images/img-login.png';


class Login extends React.Component<any, any>{
        constructor(props: any) {
                super(props)
        }

        public render() {
                return (
                        <div>
                                <div className='content-login row d-fl-ac'>
                                        <div className="d-none d-md-flex col-md-6">
                                                <img className='imagen' src={LoginImg} alt="" />
                                        </div>
                                        <div className="col-12 col-md-6">
                                                <div className='box-login  fl-dir-c d-fl-ac-se'>
                                                        <p className='f-45 c-title'>Real Time</p>
                                                        <div className='box-input d-fl-ac fl-dir-c'>
                                                                <input className="pad-elem" type="text" />
                                                                <input className="pad-elem" type="password" />
                                                                <button className="pad-elem" type='button'>Iniciar Sesión</button>
                                                        </div>
                                                        <div className="separator">
                                                                <div className="line"></div>
                                                                <div className="circlee">o</div>
                                                                <div className="line"></div>
                                                        </div>
                                                        <button className="btn-facebook d-fl-ac" type="button">
                                                                <span className="icon-fb"></span>
                                                                <span className="textFacebook">Iniciar sesión con Facebook</span>
                                                        </button>

                                                        <a className="forgetPass" href="#" >¿Olvidaste tu contraseña?</a>

                                                </div>
                                                <div className="register fl-dir-c d-fl-ac-jc">
                                                        <p>
                                                                ¿No tienes una cuenta?
                                                                 <a href=""><span className="regist">   Regístrate</span></a>
                                                        </p>
                                                </div>
                                                <div className="download">
                                                        <p>Descargar app</p>
                                                </div>
                                        </div>
                                </div>


                        </div>
                )
        }
}

export default Login;