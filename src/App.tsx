import React from 'react';
import { Router, Route } from 'react-router-dom';
import history from './history/history';
import './App.css';
import Main from './views/Main/Main';
import { withRouter, Switch } from 'react-router';

class App extends React.Component<any, any>{
	constructor(props: any) {
		super(props);
		this.state = {
			ifLayout: 1
		}
	}
	// función de tipo component route, se usa para definir un layout especifico para determinar vistas
	public MainRoute = ({ component: Component, layout: Layout, outPutProps: OutPutProps, ifLayout: IfLayout, ...rest }: any) => {
		if (IfLayout === 1) {
			return (
				<Route
					exact
					{...rest}
					render={(props: any) => (
						<Layout>
							<Component
								outPutProps={OutPutProps}
								{...props}
							/>
						</Layout>
					)}
				/>
			)
		} else {
			return <></>
		}
	}

	public MainLayout = (props: any) => {
		return (
			<div className='general-content'>
				<div className='container-fluid content-main'>
					{props.children}
				</div>
			</div>
		)
	}

	public OutPutProps() {
		console.log()
	}


	public render() {
		return (
			<div className="App">
				<Router history={history}>
					<Switch>
						<this.MainRoute
							exact
							patch='/'
							layout={this.MainLayout}
							outPutProps={this.OutPutProps}
							ifLayout={this.state.ifLayout}
							component={Main}
						/>
					</Switch>
				</Router>
			</div>
		)
	}

}

export default (withRouter(App));