import { resourceUsage } from "process";
import * as React from "react";
import Calculator from "../../components/Calculator/Calculator";
import ListsCoint from "../../components/ListsCoins/ListsCoins";
import './Main.css';


export const Tabs = () => {
        const [state, setState] = React.useState(true);
        console.log(state);

        return (
                <>
                        <div className="menus">
                                <div className={"tabs tab1 " + (state ? "active" : "")} onClick={() => { setState(!state) }}>REALIZAR CAMBIO</div>
                                <div className={"tabs tab2 " + (!state ? "active" : "")} onClick={() => { setState(!state) }}>LISTADO DE MONEDAS</div>
                        </div>
                        {
                                state && <Calculator />
                        }

                        {
                                !state && <ListsCoint />
                        }

                </>
        )
}

class Main extends React.Component<any, any>{

        constructor(props: any) {
                super(props);
                this.state = {
                        showToogle: false
                }

                // this.toggle = this.toggle.bind(this);
        }



        // public toggle() {

        // }


        public render() {
                const { state } = this.state;

                return (
                        <div className='container-main'>
                                <Tabs />
                        </div>
                )
        }
}

export default Main;